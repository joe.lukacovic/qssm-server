FROM debian:sid

RUN apt-get update && apt-get install -y libsdl2-dev libmad0-dev libopus-dev libvorbis-dev zlib1g-dev libvorbisfile3 libopusfile-dev libcurl4-openssl-dev zip

RUN mkdir /app

WORKDIR /app

COPY QSS-M-l64 /app

RUN chmod u+x QSS-M-l64

ENTRYPOINT [ "./QSS-M-l64", "-mem", "256", "-dedicated", "-protocol", "666", "-condebug" ]